﻿using UnityEngine;
using System.Collections;

public class TickManager : MonoBehaviour {

    public delegate void TickEvent();
    public static event TickEvent workTick;

    private float timer;
    public float tickTime = 1f;
    
    void Update()
    {
        if(timer <= 0)
        {
            if (workTick != null)
                workTick();

            timer = tickTime;
        }
        else
        {
            timer -= Time.deltaTime;
        }
    } 
}
