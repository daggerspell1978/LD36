﻿//Twitch Mareske
//Discord Maer

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {
    //rotation is still bugged, need to fix it.

    #region Variables

    #region Day
    public static int curDay = 0;
    [Header("Day")]
    [Tooltip("Drag me in HD, i am the Time Manager. You need nothing else to do, if you want feedback, then attach a Image at the Referenzes region.")]
    public static TimeOfDay curTimeOfDay; //Current time of day we have
                                          //How many days have passed / On wich day we are
    #endregion Day

    #region Light
    public float colorChangeTime = 0.5f;
    public DaylightBehaviour eveningLight;
    [Header("Light")]
    public float lightChangeTime = 8f; //How long the Directional light needs to change to its desired Direction
    public DaylightBehaviour middayLight;
    //How long the Color takes to change to the target color
    public DaylightBehaviour morningLight; //See external class on the Bottom
                                           //See external class on the Bottom
                                           //See external class on the Bottom
    public DaylightBehaviour nightLight; //See external class on the Bottom
    #endregion Light

    #region Seasons
    [Header("Seasons")]
    public static Season curSeason; //The current season we are in atm
    public int daysPerSeason = 10;
    private int seasonDays = 0; //How many days we had in this season
                                //How much days a season has
    #endregion Seasons

    #region Timer
    [Header("Timer")]
    public float curTimer = 0;
    public int eveningTime = 10;
    public int middayTime = 10;
    public int morningTime = 10; //How long the morning lasts
                                 //How long the midday lasts
                                 //How long the evening lasts
    public int nightTime = 10; //How long the night lasts
    #endregion Timer

    #region Referenzes
    [Header("Referenzes")]
    public Camera myCam; //The Main camera that we use for the Game
    public Light myLight; //The Main directional light or sun that we use
    #endregion Referenzes

    #region Events
    //Events
    public delegate void TimeEvent(TimeOfDay _dayTime, Season _season);
    public static event TimeEvent onDayTimeChanged; //Fires when we chage the daytime
    public static event TimeEvent onSeasonChanged; //Fires when we change the season

    static TimeManager() {
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
    }

    private static void SceneManager_sceneUnloaded(Scene arg0) {
        onDayTimeChanged = null;
        onSeasonChanged = null;
        curDay = 0;
        curSeason = Season.Summer;
        curTimeOfDay = TimeOfDay.Morning;
    }

    #endregion Events

    #endregion Variables

    #region Functions

    #region Unity Standart Functions

    void Start() {
        //Assing everythong at the start so it "looks" like morning
        myLight.intensity = morningLight.intensity;
        myLight.color = morningLight.color;
        iTween.MoveTo(myLight.gameObject, morningLight.rotation, lightChangeTime);
        curTimer = morningTime;

        if(onDayTimeChanged != null)
            onDayTimeChanged(curTimeOfDay, curSeason);

        if(onSeasonChanged != null)
            onSeasonChanged(curTimeOfDay, curSeason);
    }

    void Update() {
        Timer(); //The timer that counts the Time (You dont say)
        SmoothTransition(); //Smooth transition from one daytime to another
    }

    #endregion Unity Standart Functions

    #region Timer and Smoothtrans running in Update

    void SmoothTransition() {
        //Changing stuff depent on what time of day it is
        //Directional light intensity
        //Directional light color
        //Cam background color
        //and Directional light rotation

        switch(curTimeOfDay) {
        case TimeOfDay.Morning:
            myLight.intensity = iTween.FloatUpdate(myLight.intensity, morningLight.intensity, lightChangeTime);
            myLight.color = Color.Lerp(myLight.color, morningLight.color, Time.deltaTime * colorChangeTime);
            myCam.backgroundColor = Color.Lerp(myCam.backgroundColor, morningLight.background, Time.deltaTime * colorChangeTime);
            iTween.RotateUpdate(myLight.gameObject, morningLight.rotation, lightChangeTime);
            break;
        case TimeOfDay.Midday:
            myLight.intensity = iTween.FloatUpdate(myLight.intensity, middayLight.intensity, lightChangeTime);
            myLight.color = Color.Lerp(myLight.color, middayLight.color, Time.deltaTime * colorChangeTime);
            myCam.backgroundColor = Color.Lerp(myCam.backgroundColor, middayLight.background, Time.deltaTime * colorChangeTime);
            iTween.RotateUpdate(myLight.gameObject, middayLight.rotation, lightChangeTime);
            break;
        case TimeOfDay.Evening:
            myLight.intensity = iTween.FloatUpdate(myLight.intensity, eveningLight.intensity, lightChangeTime);
            myLight.color = Color.Lerp(myLight.color, eveningLight.color, Time.deltaTime * colorChangeTime);
            myCam.backgroundColor = Color.Lerp(myCam.backgroundColor, eveningLight.background, Time.deltaTime * colorChangeTime);
            iTween.RotateUpdate(myLight.gameObject, eveningLight.rotation, lightChangeTime);
            break;
        case TimeOfDay.Night:
            myLight.intensity = iTween.FloatUpdate(myLight.intensity, nightLight.intensity, lightChangeTime);
            myLight.color = Color.Lerp(myLight.color, nightLight.color, Time.deltaTime * colorChangeTime);
            myCam.backgroundColor = Color.Lerp(myCam.backgroundColor, nightLight.background, Time.deltaTime * colorChangeTime);
            iTween.RotateUpdate(myLight.gameObject, nightLight.rotation, lightChangeTime);
            break;
        default:
            break;
        }
    }

    void Timer() {
        //Simple Timer
        //But why did you idiot not use Courutines.
        //Because they are frame dependent, this system not so much

        if(curTimer <= 0) {
            ChangeTimeOfDay();
        } else {
            curTimer -= Time.deltaTime;
        }
    }

    #endregion Timer and Smoothtrans running in Update

    #region Changes

    void ChangeSeason() {
        curSeason++;

        //Checking if we finished the Year
        if((int)curSeason >= 4)
            curSeason = Season.Spring;

        //Checking if the Event has subscriber
        if(onSeasonChanged != null)
            onSeasonChanged(curTimeOfDay, curSeason);

        Debug.Log("Changed Season to " + curSeason);
    }

    void ChangeTimeOfDay() {
        //Making a temp time of day variable to check some stuff with it
        TimeOfDay tempTimeOfDay = curTimeOfDay + 1;

        //We check if we have night or something higher than night
        //If yes we go back to morning and triggering the Next Day
        if((int)tempTimeOfDay >= 4) {
            tempTimeOfDay = TimeOfDay.Morning;
            NextDay();
        }

        //Assing the temp to the real one
        curTimeOfDay = tempTimeOfDay;

        //Reset the timer and feedback the image
        switch(curTimeOfDay) {
        case TimeOfDay.Morning:
            curTimer = morningTime;
            break;
        case TimeOfDay.Midday:
            curTimer = middayTime;
            break;
        case TimeOfDay.Evening:
            curTimer = eveningTime;
            break;
        case TimeOfDay.Night:
            curTimer = nightTime;
            break;
        default:
            break;
        }

        //Check if someone subscibed the event and when yes then fire it
        if(onDayTimeChanged != null)
            onDayTimeChanged(curTimeOfDay, curSeason);
        Debug.Log("Changed daytime to " + curTimeOfDay);
    }

    void NextDay() {
        //Wont explain this, you should not read code if you dont know what this does
        curDay++;
        seasonDays++;

        //Check if we need to change the season
        if(seasonDays >= daysPerSeason) {
            seasonDays = 0;
            ChangeSeason();
        }

        Debug.Log("You survived this night and the light of the next day shines upon you.");
    }

    #endregion Changes

    #endregion Functions
}

#region Extra Classes
[System.Serializable]
public class DaylightBehaviour {
    public Color background;
    public Color color;
    //Color of the Directional Light
    //Color of the Camera Background
    public float intensity;
    public Vector3 rotation; //Rotation of the Directional Light
                             //Intensity of the Directional Light
}
public enum Season //Different Seasons that are gonna be in the Game
{
    Spring,
    Summer,
    Autumn,
    Winter
}

public enum TimeOfDay //Different Time of days for the game
{
    Morning,
    Midday,
    Evening,
    Night
}

#endregion Extra Classes