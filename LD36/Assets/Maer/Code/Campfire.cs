﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections;

public class Campfire : MonoBehaviour {

    public int campfireLife = 10;

    public void OnTriggerEnter(Collider _other)
    {
        _other.SendMessage("Die", SendMessageOptions.DontRequireReceiver);
        ReduceLife();
    }

    void ReduceLife()
    {
        if (campfireLife <= 0)
            return;

        campfireLife--;

        if (campfireLife <= 0)
            Debug.Log("Oh my god! Hardly how can you let them destroy your Campfire, what are you a campfire hater?");
    }
}
