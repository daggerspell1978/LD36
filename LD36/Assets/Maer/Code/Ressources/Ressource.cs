﻿using System;
using System.Collections;
using UnityEngine;

public class Ressource : MonoBehaviour {
    public int curHitpoints = 10;

    public bool destroyObject = true;
    //how hard is it to harvest this ressource?
    public int maxHitpoints = 10;
    //What should we drop
    protected GameObject highlight;
    BasicWorker _curWorker;
    [Header("Ressource")]
    private bool addedToList = false;
    public BasicWorker curWorker {
        get {
            return _curWorker;
        }
        set {
            if(value == null && _curWorker != null) {
                _curWorker.curRessource = null;
                _curWorker = value;
                StopHarvestEffects();
            }
            _curWorker = value;
        }
    }

    public virtual void Awake() {
        curHitpoints = maxHitpoints;
    }

    //If this ressource got added to the list, to get harvested
    /// <summary>
    /// Work this ressource, happens every tick
    /// </summary>
    /// <param name="_strengh"></param>
    public void harvest(BasicWorker worker, int _strengh) {
        worker.transform.rotation = Quaternion.LookRotation(transform.position - worker.transform.position);
        if(DateTime.Now - lastTimeAudioStarted > TimeSpan.FromSeconds(harvestSound.length + 2)) {
            currentPlayingSound = SharedResources.PlaySoundEffect(harvestSound, harvestSoundVolume);
            lastTimeAudioStarted = DateTime.Now;
        }

        curHitpoints -= _strengh;

        if(curHitpoints <= 0) {
            lastTimeAudioStarted = DateTime.MinValue;
            HarvestActionComplete(worker);
        }
    }

    public void HarvestCompleteNoLoot(float time = 0) {
        curWorker.ResourceCollectionComplete();
        Invoke("DestroyGameObject", time);
        curWorker = null;
    }

    public void HarvestCompleteSpawnLoot(float time = 0) {
        curWorker.ResourceCollectionComplete();
        float delta = 0;
        var count = UnityEngine.Random.Range(dropMinCount, dropMaxCount + 1);
        for(int i = 0; i < count; i++) {
            var logDeltaPosition = transform.up * delta;
            logDeltaPosition = new Vector3(logDeltaPosition.x, 0, logDeltaPosition.z);
            Instantiate(drop, transform.position +
                new Vector3(0, 1, 0) + logDeltaPosition,
                 Quaternion.Euler(transform.rotation.eulerAngles
                 + new Vector3(0, UnityEngine.Random.Range(-30, 30), 0)));
            delta += drop.GetComponentInChildren<BoxCollider>().size.y
                * drop.transform.GetChild(0).localScale.y * .7f;
        }
        Invoke("DestroyGameObject", time);
        curWorker = null;
    }

    /// <summary>
    /// If this object gets clicked with the mouse
    /// </summary>
    public void OnMouseDown() {
        if(curWorker == null) {
            Highlight();
        }
    }

    internal virtual void StartHarvestEffects() {
        UnHighlight();
    }

    internal virtual void StopHarvestEffects() {
        UnHighlight();
        curWorker = null;
    }

    protected virtual void HarvestActionComplete(BasicWorker worker) {
        RessourceManager.me.RemoveRessource(this);
    }

    #region Drops

    public GameObject drop;
    public int dropMaxCount;
    public int dropMinCount;

    #endregion Drops

    #region Audio

    [Header("Audio")]
    public AudioClip harvestSound;

    public float harvestSoundVolume;
    private GameObject currentPlayingSound;
    private DateTime lastTimeAudioStarted;

    #endregion Audio

    private void DestroyGameObject() {
        if(destroyObject)
            Destroy(gameObject);
    }

    private void GenerateHighlight() {
        highlight = HighlightCreator.Create(gameObject);
    }

    /// <summary>
    /// Highlight the resource and add to resource manager
    /// </summary>
    private void Highlight() {
        if(!addedToList) {
            if(highlight == null) {
                GenerateHighlight();
            }
            highlight.SetActive(true);
            RessourceManager.me.AddRessource(this);
            addedToList = true;
        }
    }

    /// <summary>
    /// UnHighlight the resource and remove from resource manager
    /// </summary>
    private void UnHighlight() {
        if(currentPlayingSound != null) {
            Destroy(currentPlayingSound);
        }
        if(addedToList) {
            // do we need this?  curWorker = null;
            highlight.SetActive(false);
            addedToList = false;
        }
        RessourceManager.me.RemoveRessource(this);
    }
}