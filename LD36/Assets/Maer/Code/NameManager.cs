﻿//Twitch Mareske
//Discord Maer

using System.Linq;
using UnityEngine;

public class NameManager : MonoBehaviour {
    public static NameManager me;

    public string GetRandomName() {
        return Credits.CreditsDictionary.creditsDictionary.Values.ToArray()[Random.Range(0,
            Credits.CreditsDictionary.creditsDictionary.Count)].Name;
    }

    void Awake() {
        me = this;
    }
}