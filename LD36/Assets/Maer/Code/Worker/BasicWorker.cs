﻿//Twitch Mareske
//Discord Maer

using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(WorkerMovement))]
[RequireComponent(typeof(WorkerNeeds))]
public class BasicWorker : MonoBehaviour {

    #region Variables

    public bool debug = false;
    //Checks if we want to display the Debug logs
    public int harvestDistance = 3;
    [Header("Harvesting")]
    public int harvestStrengh = 1;
    public WorkerInventory inventory;
    public bool isWaitingForWork = true;
    //Current Ressource we want to harvest
    [HideInInspector]
    public WorkerMovement myMovement;
    [HideInInspector]
    public WorkerNeeds myNeeds;
    public TextMesh nameDisplay;
    //Referenz to Worker needs logic
    [Header("Name")]
    public string wName;
    Ressource _curRessource;
    //Referenz to Worker movement logic
    private Transform campfire;
    public Ressource curRessource {
        get {
            return _curRessource;
        }
        set {
            var temp = _curRessource;
            _curRessource = value;
            if(value != temp && temp != null) {
                temp.curWorker = null;
                isWaitingForWork = true;
            }
            //DebugConfirmCleared(temp);
            DebugConfirmResourceCleared(temp);
        }
    }

    [System.Diagnostics.Conditional("DEBUG")]
    private void DebugConfirmCleared(Ressource temp) {
        if(temp != null) {
            if(!(temp.curWorker != this || curRessource == temp)) {
                Debug.Assert(false);
            }
        }
    }

    [System.Diagnostics.Conditional("DEBUG")]
    private void DebugConfirmResourceCleared(Ressource temp) {
        StartCoroutine(DebugConfirmResourceClearedRoutine(temp));
    }

    private IEnumerator DebugConfirmResourceClearedRoutine(Ressource temp) {
        yield return new WaitForSeconds(.1f);
        DebugConfirmCleared(temp);
    }

    //Are we idling and waiting for work?

    //More strengh = fast tree chopping
    //How far we can stand away from the object we want to harvest

    //campfire referenz to let the worker go back to the camp and wait there for new work

    /* ADDED BY STATH*/
    /* END OF STATH AREA*/

    //The name of the Worker
    //The name display, so that the players can see it

    #endregion Variables

    static int playerId = 0;
    static int priority = 20;

    /// <summary>
    /// Let the worker get back to the camp to wait there and chill
    /// </summary>
    public void MoveToCamp() {
        myMovement.MoveTo(campfire);
    }

    private IEnumerator PlayIntroScript(int playerId) {
        switch(playerId) {
        case 0:
            ChatBubbleManager.DisplayChatBubble(transform, "Things are so hard in the middle of nowhere", 3, Vector3.zero, Color.black);
            yield return new WaitForSeconds(16);
            ChatBubbleManager.DisplayChatBubble(transform, "We need to keep this burning", 3, Vector3.zero, Color.black);

            break;
        case 1:
            yield return new WaitForSeconds(2);
            ChatBubbleManager.DisplayChatBubble(transform, "Nobody here even knows how to start a fire!", 4, Vector3.zero, Color.black);

            yield return new WaitForSeconds(10);
            ChatBubbleManager.DisplayChatBubble(transform, "Whoa, that was close!", 2, Vector3.zero, Color.black);
            yield return new WaitForSeconds(9);
            ChatBubbleManager.DisplayChatBubble(transform, "Quick, let's gather wood for the fire!", 6, Vector3.zero, Color.black);

            break;
        case 2:
            yield return new WaitForSeconds(5);
            ChatBubbleManager.DisplayChatBubble(transform, "Fire keeps us safe and warm", 3, Vector3.zero, Color.black);
            yield return new WaitForSeconds(14);
            ChatBubbleManager.DisplayChatBubble(transform, "It's going to get cold soon", 3, Vector3.zero, Color.black);

            break;
        }
    }

    private void Start() {
        //Get referenzes
        myMovement = GetComponent<WorkerMovement>();
        myNeeds = GetComponent<WorkerNeeds>();
        myNeeds.myWorker = this;
        campfire = GameObject.FindGameObjectWithTag("Campfire").transform;

        //Stath again
        inventory = GetComponent<WorkerInventory>();
        Debug.Assert(inventory != null);

        //Adding worker to temp Manager
        TemperatureManager.me.AddWorker(this);

        //Gettin a new name for the worker
        wName = NameManager.me.GetRandomName();
        nameDisplay.text = wName;
        gameObject.name = wName;

        var navMesh = GetComponent<NavMeshAgent>();
        navMesh.avoidancePriority = priority++;

        StartCoroutine(PlayIntroScript(playerId++));
    }

    #region Event

    private Ressource lastResource;

    public void GetTick() {
        if(lastResource != null && lastResource != curRessource) {
            lastResource.StopHarvestEffects();
        }

        if(isWaitingForWork) {
            GetWork();
        } else {
            if(GetDistance() <= harvestDistance) {
                if(lastResource != curRessource) {
                    curRessource.StartHarvestEffects();
                }

                HarvestRessource();
            } else {
                if(!myMovement.IsMoving) {
                    Debug.Log("Worker got stuck, resetting target");
                    curRessource = null;
                    GetWork();
                }
            }
        }

        lastResource = curRessource;
    }

    private void OnDisable() {
        TickManager.workTick -= GetTick;
    }

    private void OnEnable() {
        TickManager.workTick += GetTick;
    }

    #endregion Event

    #region Work

    public void SetResourceToCollect(Ressource ressource) {
        if(curRessource != null) {
            curRessource.curWorker = null;
        }
        curRessource = ressource;
        if(debug)
            Debug.Log("Got new Resource!");

        curRessource.curWorker = this;
        isWaitingForWork = false;
        MoveToRessource();
    }

    /// <summary>
    /// The brain of the Worker, trying to get work with each tickevent
    /// </summary>
    private void GetWork() {
        var ressource = RessourceManager.me.GetRessource(this);

        if(ressource != null) {
            SetResourceToCollect(ressource);
        } else {
            isWaitingForWork = true;
            MoveToCamp();
        }
    }

    #endregion Work

    #region Ressource harvesting

    /// <summary>
    /// Feedback from the Ressource when its harvested
    /// </summary>
    public void ResourceCollectionComplete() {
        if(curRessource != null) {
            curRessource.curWorker = null;
        }
        if(debug)
            Debug.Log("Finished Harvesting Resource!");
        curRessource = null;
        isWaitingForWork = true;
    }

    /// <summary>
    /// Calculating Distance beetween worker and ressource
    /// </summary>
    /// <returns></returns>
    private float GetDistance() {
        return Vector3.Distance(this.transform.position, curRessource.transform.position);
    }

    /// <summary>
    /// Do your harveststrengh to the current ressource, to chop that Mothertrucker down
    /// </summary>
    private void HarvestRessource() {
        if(debug)
            Debug.Log("Harvesting Resource!");
        curRessource.harvest(this, harvestStrengh);
    }

    /// <summary>
    /// Move to the current ressource
    /// </summary>
    private void MoveToRessource() {
        if(debug)
            Debug.Log("Moving to Resource!");
        myMovement.MoveTo(curRessource.transform);
    }

    #endregion Ressource harvesting
}