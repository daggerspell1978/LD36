﻿//Twitch Mareske
//Discord Maer

using System;
using System.Collections;
using UnityEngine;

public class Chatter : MonoBehaviour {
    public float chatterCD = 2f;
    public Transform textParent;
    public GameObject textPrefab;
    private Transform campFire;
    private bool canChat = true;
    private bool inList = false;

    #region Serious

    public void AddText(string _text, Color _color) {
        GameObject go = Instantiate(textPrefab, new Vector3(textParent.position.x, textParent.position.y + 3, textParent.position.z), Quaternion.identity) as GameObject;
        go.transform.SetParent(this.transform);
        go.GetComponent<ChatText>().SetText(_text, this.transform, _color);
        StartChatterCD();
    }

    public bool CanWeChat() {
        return canChat;
    }

    public void StartChatterCD() {
        canChat = false;
        StartCoroutine(ChatterCD());
    }

    IEnumerator ChatterCD() {
        yield return new WaitForSeconds(chatterCD);
        canChat = true;
    }

    void Start() {
        campFire = GameObject.FindGameObjectWithTag("Campfire").transform;
    }

    #endregion Serious

    #region Fun
    public AudioClip mumble;

    public void FunMode() {
        if(Vector3.Distance(this.transform.position, campFire.transform.position) <= 10) {
            if(!inList) {
                FunManager.AddChatter(this.gameObject);
                inList = true;
            }

            if(CanWeChat()) {
                string chat = FunManager.GetChat(this.gameObject);

                if(chat != null) {
                    SharedResources.PlaySoundEffect(mumble, .1f, UnityEngine.Random.Range(.5f, 1.5f));
                    AddText(chat, Color.white);
                }
            }
        } else {
            if(inList) {
                FunManager.RemoveChatter(this.gameObject);
                inList = false;
            }
        }
    }

    #endregion Fun
}