﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class WorkerMovement : MonoBehaviour {
    public Transform curTarget;
    public float stoppingDistance = 0f;
    private NavMeshAgent agent;

    public bool IsMoving {
        get {
            return agent.velocity.magnitude > 0;
        }
    }

    public void MoveTo(Transform _target) {
        curTarget = _target;
        agent.SetDestination(_target.position);
        agent.Resume();
    }

    public void MoveTo(Vector3 _position)
    {
        curTarget = null;
        agent.SetDestination(_position);
        agent.Resume();
    }

    float DistanceToTraget() {
        return Vector3.Distance(transform.position, curTarget.position);
    }

    void Start() {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update() {
        if(curTarget != null) {
            if(DistanceToTraget() <= stoppingDistance) {
                agent.Stop();
                curTarget = null;
            }
        }
    }
}