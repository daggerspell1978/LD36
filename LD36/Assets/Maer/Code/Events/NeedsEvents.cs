﻿using UnityEngine;
using System.Collections;

public static class NeedsEvents{

    public delegate void NeedsEvent(BasicWorker _worker);
    public static event NeedsEvent onWorkerFreezing;
    public static event NeedsEvent onWorkerBurning;
    public static event NeedsEvent onWorkerStarving;

    /// <summary>
    /// Fires the worker freezing event
    /// </summary>
    /// <param name="_worker"></param>
    public static void WorkerFreezing(BasicWorker _worker)
    {
        if(onWorkerFreezing != null)
            onWorkerFreezing(_worker);
    }

    /// <summary>
    /// Fires the worker burning event
    /// </summary>
    /// <param name="_worker"></param>
    public static void WorkerBurning(BasicWorker _worker)
    {
        if (onWorkerBurning != null)
            onWorkerBurning(_worker);
    }

    /// <summary>
    /// Fires the worker is starving event
    /// </summary>
    /// <param name="_worker"></param>
    public static void WorkerStarving(BasicWorker _worker)
    {
        if (onWorkerStarving != null)
            onWorkerStarving(_worker);
    }
}
