﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections.Generic;
using System;

public class TemperatureManager : MonoBehaviour
{
    public List<HeatSource> heatSources = new List<HeatSource>(); //All the objects that give heat in the scene, dont forget to add the Heatsource script to a object.
    public List<BasicWorker> workers = new List<BasicWorker>(); //All the workers that we have at the moment, a worker manager would be better later but for now it is ok

    public static TemperatureManager me;

    #region Events
    void OnEnable()
    {
        TickManager.workTick += GetTick;
    } //subscribe the Event

    void OnDisable()
    {
        TickManager.workTick -= GetTick;
    } //unsubscribe the Event

    void GetTick()
    {
        CalculateHeat();
    }
    #endregion

    void Awake()
    {
        me = this;
    }

    #region Adding and Removing
    /// <summary>
    /// Add a Worker to the WorkerList
    /// </summary>
    /// <param name="_worker"></param>
    public void AddWorker(BasicWorker _worker)
    {
        workers.Add(_worker);
    } 

    /// <summary>
    /// Add objects that gives Heat to the heatSource List
    /// </summary>
    /// <param name="_this"></param>
    public void AddHeatSource(HeatSource _this)
    {
        heatSources.Add(_this);
    }

    /// <summary>
    /// Remove objects from the Heat Source list
    /// </summary>
    /// <param name="_this"></param>
    public void RemoveHeatSource(HeatSource _this)
    {
        heatSources.Remove(_this);
    }
    #endregion

    /// <summary>
    /// Calculates the Heat level for the Workers
    /// </summary>
    void CalculateHeat()
    {
        foreach (BasicWorker wario in workers)
        {
            float tempHeat = 0; 

            foreach (HeatSource hs in heatSources)
            {
                float distance = Vector3.Distance(wario.transform.position, hs.transform.position); //The distance to the Heatsource

                if (distance <= hs.heatRange) //If we are in the heatrange
                {
                    int heatGain = (int)(hs.heatGeneration - (distance / 2)); //Calculate that it gets hotter near the source and colder when we are away from it
                    if (heatGain <= 0)//If its less then zero then zero it out
                        heatGain = 0;
                    tempHeat += heatGain; //Add heat from this heartsource to the heat, that the worker gains later
                }
            }

            wario.myNeeds.SetTemperature((int)tempHeat); //Give the worker a new heat value
        }
    }
}
