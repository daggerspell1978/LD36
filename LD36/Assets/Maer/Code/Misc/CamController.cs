﻿//Twitch Mareske
//Discord Maer

using System.Collections;
using UnityEngine;

public class CamController : MonoBehaviour {
    [Header("Movement")]
    public float camSpeed = 10;
    public float lerpTime = 5;
    public int maxZoomIn = 5;
    public int maxZoomOut = 30;
    [Header("Zoom")]
    public float zoomSpeed = 10;
    Vector3 defaultPosition;
    private Transform myTrans;
    private Transform smoother;
    private float zoomState;

    void CamMovement() {
        if(Input.GetButton("Jump")) {
            myTrans.position = defaultPosition;
        }
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel") * -1;

        horizontal = horizontal * camSpeed;
        vertical = vertical * camSpeed;
        zoomState = zoomState + (mouseScroll * zoomSpeed);

        if(zoomState < maxZoomIn)
            zoomState = maxZoomIn;

        if(zoomState > maxZoomOut)
            zoomState = maxZoomOut;

        smoother.position = new Vector3(myTrans.position.x + horizontal, zoomState, myTrans.position.z + vertical);
        iTween.MoveUpdate(gameObject, smoother.position, lerpTime);
    }

    void Start() {
        myTrans = this.transform;
        zoomState = myTrans.position.y;
        smoother = myTrans.FindChild("Smoother");
        smoother.SetParent(null);
        defaultPosition = myTrans.position;
    }

    // Update is called once per frame
    void Update() {
        CamMovement();
    }
}