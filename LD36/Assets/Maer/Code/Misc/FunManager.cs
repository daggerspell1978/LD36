﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunManager : MonoBehaviour {
    public static List<GameObject> chatter = new List<GameObject>();
    public static List<string> chatting = new List<string>();
    public static GameObject lastChatter;

    static FunManager() {
        chatting.Add("Did you heard of Ludum dare?");
        chatting.Add("Nope, what is this?");
        chatting.Add("A game jam, where HD is working on a Game.");
        chatting.Add("Good to know.");
        chatting.Add("Did you know that the bees are dying out?");
        chatting.Add("Nope, but why should i care?");
        chatting.Add("Because if the bees die, most of our current food dies with them.");
        chatting.Add("That is terrible, we need to save them");
        chatting.Add("Yeah, save the bees.");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
    }

    public static void AddChatter(GameObject _chatter) {
        chatter.Add(_chatter);
    }

    public static string GetChat(GameObject _chatter) {
        if(lastChatter != _chatter) {
            if(chatter.Count > 1) {
                if(UnityEngine.Random.Range(0, 100) == 0) {
                    string tempString = chatting[UnityEngine.Random.Range(0, chatting.Count)];
                    lastChatter = _chatter;
                    return tempString;
                }
            }
        }

        return null;
    }

    public static void RemoveChatter(GameObject _chatter) {
        chatter.Remove(_chatter);
    }
}