﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections;

public class DelayedDestroyer : MonoBehaviour {

    public float waitTime = 5f;

    void Start()
    {
        StartCoroutine(WaitAndDestroy());
    }

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
    }
}
