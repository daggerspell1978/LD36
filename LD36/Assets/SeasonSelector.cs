﻿using System.Collections;
using UnityEngine;

public class SeasonSelector : MonoBehaviour {
    Transform summer, winter;

    // Use this for initialization
    void Start() {
        winter = transform.FindChild("Winter");
        summer = transform.FindChild("Summer");
        TimeManager.onSeasonChanged += TimeManager_onSeasonChanged;
        TimeManager_onSeasonChanged(TimeManager.curTimeOfDay, TimeManager.curSeason);
    }

    private void TimeManager_onSeasonChanged(TimeOfDay _dayTime, Season _season) {
        if(winter != null) {
            winter.gameObject.SetActive(_season == Season.Winter);
        }
        if(summer != null) {
            summer.gameObject.SetActive(_season == Season.Summer);
        }
    }

    // Update is called once per frame
    void Update() {
    }
}