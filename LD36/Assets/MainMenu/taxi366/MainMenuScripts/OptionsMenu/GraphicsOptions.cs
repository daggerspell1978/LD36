﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsOptions : MonoBehaviour {
    //AA
    public Dropdown aADropdown;
    //Anisotropic
    public Dropdown anisotropicDropdown;
    //is the Game Fulscreen?
    public bool fullscreen = true;
    public Toggle fullscreenToggleButton;

    //refreshrate
    public Dropdown refreshRateDropdown;
    //TexQ
    public Dropdown textureQualityDropdown;
    //VSync
    public Dropdown vSyncDropdown;
    int[] aAOptions = new int[4];
    string[] anisotropicOptions = new string[3];
    int[] refreshRates = new int[3];
    string[] textureQOptions = new string[4];
    string[] vSyncOptions = new string[3];

    public void FirstLoad() {
        if(Screen.fullScreen) {
            fullscreen = true;
            fullscreenToggleButton.isOn = false;
        } else {
            fullscreen = false;
            fullscreenToggleButton.isOn = true;
        }

        RefreshRate();
        VSyncOptions();
        TextureQuality();
        AA();
        AnisotropicFiltering();
    }

    public void UpdateSettings() {
        //sets the fullscreen
        Screen.fullScreen = !fullscreen;
        //sets the resolution and the refresh rate
        Screen.SetResolution(Screen.width, Screen.height, fullscreen, refreshRates[refreshRateDropdown.value]);

        //sets the quality settings
        QualitySettings.vSyncCount = vSyncDropdown.value;
        QualitySettings.masterTextureLimit = textureQualityDropdown.value;
        QualitySettings.antiAliasing = aAOptions[aADropdown.value];
        QualitySettings.anisotropicFiltering = (AnisotropicFiltering)anisotropicDropdown.value;
    }

    //sets the dropdown for AA with the default level of 2x MSAA
    void AA() {
        aADropdown.ClearOptions();

        aAOptions[0] = 0;
        aAOptions[1] = 2;
        aAOptions[2] = 4;
        aAOptions[3] = 8;

        foreach(int aALvl in aAOptions) {
            if(aALvl == 0) {
                aADropdown.options.Add(new Dropdown.OptionData() { text = "Disabled" });
            } else {
                aADropdown.options.Add(new Dropdown.OptionData() { text = aALvl.ToString() + "x MSAA" });
            }
        }

        aADropdown.value = 0;
        aADropdown.value = 1;
    }

    //sets the dropdown for the AF options with a default level of all textures
    void AnisotropicFiltering() {
        anisotropicDropdown.ClearOptions();

        anisotropicOptions[0] = "Disabled";
        anisotropicOptions[1] = "Forground Only";
        anisotropicOptions[2] = "All Textures";

        foreach(string aF in anisotropicOptions) {
            anisotropicDropdown.options.Add(new Dropdown.OptionData() { text = aF });
        }

        anisotropicDropdown.value = 0;
        anisotropicDropdown.value = 2;
    }

    //sets the default refresh rate
    void RefreshRate() {
        refreshRateDropdown.ClearOptions();

        refreshRates[0] = 30;
        refreshRates[1] = 60;
        refreshRates[2] = 144;

        foreach(int refresh in refreshRates) {
            refreshRateDropdown.options.Add(new Dropdown.OptionData() { text = (refresh.ToString() + " Hz") });
        }

        refreshRateDropdown.value = 0;
        refreshRateDropdown.value = 1;
    }

    //sets the maximum mitmap level for textures
    void TextureQuality() {
        textureQualityDropdown.ClearOptions();

        textureQOptions[0] = "Full Resolution";
        textureQOptions[1] = "Half Resolution";
        textureQOptions[2] = "Quarter Resolution";
        textureQOptions[3] = "Eighth Resolution";

        foreach(string tQOpt in textureQOptions) {
            textureQualityDropdown.options.Add(new Dropdown.OptionData() { text = tQOpt });
        }

        textureQualityDropdown.value = 1;
        textureQualityDropdown.value = 0;
    }

    void ToggleFullscreen() {
        fullscreen = !fullscreen;
    }

    //sets the VSync options defult to every VBlank
    void VSyncOptions() {
        vSyncDropdown.ClearOptions();

        vSyncOptions[0] = "No VSync";
        vSyncOptions[1] = "Every Vertical Blank";
        vSyncOptions[2] = "Every Second Vertical Blank";

        foreach(string vsopt in vSyncOptions) {
            vSyncDropdown.options.Add(new Dropdown.OptionData() { text = vsopt });
        }

        vSyncDropdown.value = 0;
        vSyncDropdown.value = 1;
    }
}