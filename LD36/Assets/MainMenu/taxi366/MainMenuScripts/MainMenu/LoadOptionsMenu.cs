﻿using UnityEngine;

public class LoadOptionsMenu : MonoBehaviour
{
    private GameObject optionsMenu;

    void Start()
    {
        optionsMenu = GameObject.Find("OptionsCanvas");
        optionsMenu.SetActive(false);
    }

    public void LoadOptMenu()
    {
        /*
        Instantiate(optionsMenu);
        Destroy(gameObject);
        */

        optionsMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}