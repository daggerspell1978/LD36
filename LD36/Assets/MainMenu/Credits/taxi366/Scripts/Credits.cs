﻿namespace Credits {
    public class Credits {
        private string name;
        private string role;

        public Credits(string name, string role) {
            this.name = name;
            this.role = role;
        }

        public string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }

        public string Role {
            get {
                return role;
            }
            set {
                role = value;
            }
        }
    }
}