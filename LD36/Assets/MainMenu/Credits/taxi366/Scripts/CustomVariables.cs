﻿namespace Credits
{
    /*
    the custom variable that is used to store the credits because lovely unity wont display a dictionary in the editor
    */

    public class CustomVariables
    {
        [System.Serializable]
        public struct Credits
        {
            public string name;
            public string role;
        }
    }
}