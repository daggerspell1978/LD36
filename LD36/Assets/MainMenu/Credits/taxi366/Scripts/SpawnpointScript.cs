﻿using System.Collections.Generic;
using UnityEngine;

namespace Credits {
    public class SpawnpointScript : MonoBehaviour {
        [System.NonSerialized]
        public bool canSpawnNewText = true;
        public List<int> dictonaryIndexes;
        public GameObject objectToSpawn;
        private float randomSpawndelay;
        private int spawnCount;

        //this exists becausei like using random numbers and to that all of the text does not happen at the same time this is also called from the MoveTextUpAndDown script
        public void SpawnDelay() {
            randomSpawndelay = Time.time + Random.Range(1f, 15f);
        }

        void Awake() {
            dictonaryIndexes.Clear();
            SpawnDelay();
        }

        void SpawnObject() {
            canSpawnNewText = false;
            GameObject spawnedObject = (GameObject)Instantiate(objectToSpawn, transform.position - new Vector3(0, 5), transform.rotation);
            //                              ^ Why do i need to cast this? Like seriously...It is just annoying

            //sets the required infomation in the spawned object
            spawnedObject.GetComponent<MoveTextUpAndDown>().mySpawnpoint = this;
            spawnedObject.GetComponent<MoveTextUpAndDown>().nameText.text = CreditsDictionary.creditsDictionary[dictonaryIndexes[spawnCount]].Name;
            spawnedObject.GetComponent<MoveTextUpAndDown>().roleText.text = CreditsDictionary.creditsDictionary[dictonaryIndexes[spawnCount]].Role;

            spawnCount++;
            if(spawnCount >= dictonaryIndexes.Count) {
                spawnCount = 0;
            }
        }

        void Update() {
            //wont try to make a thing if it was not assigned any credits data or if it has already run through all of its things
            if(dictonaryIndexes.Count > 0) {
                if(Time.time > randomSpawndelay) {
                    //makes sure that there is not already a text object moving
                    if(canSpawnNewText) {
                        SpawnObject();
                    }
                }
            }
        }
    }
}