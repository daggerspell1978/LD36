﻿using UnityEngine;
using System.Collections;

public enum BearState {
    Walk,
    Run,
    Bite,
    Slash,
    Stand,
    ReceiveHit,
    Death
}