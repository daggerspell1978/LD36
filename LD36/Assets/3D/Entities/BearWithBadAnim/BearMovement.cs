﻿using UnityEngine;

public class BearMovement : MonoBehaviour {
    public float bearCoolDown = 0.25f;
    public float moveSpeed = 0.4f;

    BearStateController bearController;
    private Vector3 target;
    private float timer;

    //sets a new movement-target for the bunnybear
    private void setTarget() {
        target = new Vector3(Random.Range(2.0f, 30.0f), 0, Random.Range(2.0f, 30.0f));
    }

    void Start() {
        bearController = GetComponent<BearStateController>();
        timer = bearCoolDown;
        setTarget();
    }

    void Update() {
        timer -= Time.deltaTime;
        this.gameObject.transform.LookAt(target);
        this.gameObject.transform.Translate(Vector3.forward * (moveSpeed * Time.deltaTime));
        if(Vector3.Distance(this.gameObject.transform.position, target) <= 2f)
            setTarget();

        if(timer <= 0) {
            bearController.animState = (BearState)Random.Range(0, 7);
            timer = bearCoolDown;
        }
    }
}