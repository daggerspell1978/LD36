﻿using System.Collections;
using UnityEngine;

public class WeatherManager : MonoBehaviour {
    public GameObject rainEffect, snowEffect, sunEffect, cloudEffect;
    private Season season;
    private TimeOfDay timeOfDay;
    public static event WeatherChangeEvent OnWeatherChanged;
    public Weather Current {
        get; protected set;
    }

    public void Awake() {
        TimeManager.onSeasonChanged += TimeManager_onSeasonChanged;
        TimeManager.onDayTimeChanged += TimeManager_onDayTimeChanged;
    }

    public void ChangeWeather(Weather _weather) {
        if(_weather == Current)
            return;

        rainEffect.gameObject.SetActive(false);
        snowEffect.gameObject.SetActive(false);
        sunEffect.gameObject.SetActive(false);
        cloudEffect.gameObject.SetActive(false);

        switch(_weather) {
        case Weather.Cloudy:
            cloudEffect.gameObject.SetActive(true);
            break;

        case Weather.Sun:
            if(timeOfDay == TimeOfDay.Night) {
                break;
            }

            sunEffect.gameObject.SetActive(true);
            break;

        case Weather.Precipitation:

            var snow = season == Season.Summer ? false : true;
            if(season == Season.Autumn || season == Season.Spring) {
                snow = Random.Range(0, 10) < 3;
            }

            rainEffect.gameObject.SetActive(!snow);
            snowEffect.gameObject.SetActive(snow);
            break;

        default:
            break;
        }

        Current = _weather;

        if(OnWeatherChanged != null)
            OnWeatherChanged(_weather);
    }

    private void TimeManager_onDayTimeChanged(TimeOfDay _dayTime, Season _season) {
        ChangeWeather((Weather)UnityEngine.Random.Range(0, 4));
    }

    private void TimeManager_onSeasonChanged(TimeOfDay _dayTime, Season _season) {
        season = _season;
        timeOfDay = _dayTime;
    }
}

public enum Weather {
    None,
    Cloudy,
    Sun,
    Precipitation
}

public delegate void WeatherChangeEvent(Weather args);