﻿public interface IInventoryItem
{
    string Name { get; }
    int Weight { get; }
}

public interface IInventoryItem<T> : IInventoryItem
{
    T Item { get; }
}