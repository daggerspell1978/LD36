﻿using UnityEngine;
using System.Collections.Generic;

public class LevelController : MonoBehaviour
{
    public int seed = 1;
    //public bool usePerlin = false;
    //public bool useBoth = true;
    public bool deleteOffScreenTiles = false;
    public int overRenderX = 3; //Renders this many tiles past the edge of the screen. This allows for generating tiles without the player catching it.
    public int overRenderY = 5;
    public float scale = 1000; //only used for perlin
    public List<BiomeController> biomes = new List<BiomeController>();//prefabs to spawn as ground.
    public int[] biomesID;
    public List<BiomeController> biomesMerged = new List<BiomeController>();
    List<CMChunkTiles> tilesToGenerate = new List<CMChunkTiles>();
    GameObject tileContainer;
    public int needToCreateCount = 0;
    public float deltaTime;
    public CMBounds screenBounds = new CMBounds();
    [Range(0,1)]
    //public float mergeStrength = .5f;

    float lastLeft, lastRight, lastTop, lastBottom;

    int chunkOffset = 1000;
    public static readonly int chunkSize = 20;
    GameObject[,] chunks = new GameObject[2000, 2000];
    public GameObject[] objectsToTrack;

    public List<GameObject> liveChunks = new List<GameObject>();

    void Start()
    {
        UpdateBounds();

        // associate an ID with each biome in the list
        biomesID = new int[biomes.Count];
        biomesMerged.Clear();
        for (int i = 0; i < biomes.Count; ++i)
        {
            bool duplicate = false;
            for (int j = 0; j < biomesMerged.Count; ++j)
            {
                if (biomesMerged[j] == biomes[i])
                {
                    duplicate = true;
                    biomesID[i] = j;
                    break;
                }
            }
            if (!duplicate)
            {
                biomesID[i] = biomesMerged.Count;
                biomesMerged.Add(biomes[i]);
            }
        }
        Debug.Log("Found "+biomesMerged.Count+" biomes");
        tileContainer = new GameObject();
        tileContainer.name = "TileContainer";
    }
    bool firstLoop = false;

    void Update()
    {
        //UpdateBounds();
        List<Vector2> chunksToMakeLive = new List<Vector2>();
        for (int i = 0; i < objectsToTrack.Length; i++)
        {
            if (objectsToTrack[i].GetComponent<Camera>() == null)
            {
                for (int renderOffsetX = -1; renderOffsetX < 1; renderOffsetX++)
                {
                    for (int renderOffsetY = -1; renderOffsetY < 1; renderOffsetY++)
                    {
                        Vector2 thisChunk = new Vector2();
                        thisChunk.x = Mathf.RoundToInt((objectsToTrack[i].transform.position.x / 2) / chunkSize) + renderOffsetX;
                        thisChunk.y = Mathf.RoundToInt((objectsToTrack[i].transform.position.z / .6f) / chunkSize) + renderOffsetY;
                        chunksToMakeLive.Add(thisChunk);
                    }
                }
            }
            else
            {
                for (int renderOffsetX = -overRenderX; renderOffsetX < overRenderX; renderOffsetX++)
                {
                    for (int renderOffsetY = -overRenderY; renderOffsetY < overRenderY; renderOffsetY++)
                    {
                        Vector2 thisChunk = new Vector2();
                        thisChunk.x = Mathf.RoundToInt((objectsToTrack[i].transform.position.x/2) / chunkSize) + renderOffsetX;
                        thisChunk.y = Mathf.RoundToInt((objectsToTrack[i].transform.position.z/.6f) / chunkSize) + renderOffsetY;
                        chunksToMakeLive.Add(thisChunk);
                    }
                }
            }
        }



        for (int i = 0; i < liveChunks.Count; i++)
        {
            bool turnOff = true;
            CMChunk currentLiveChunk = liveChunks[i].GetComponent<CMChunk>();
            for (int j = 0; j < chunksToMakeLive.Count; j++)
            {
                if(currentLiveChunk.x == chunksToMakeLive[j].x && currentLiveChunk.y == chunksToMakeLive[j].y)
                {
                    turnOff = false;
                    break;
                }
            }
            if(turnOff)
            {
                liveChunks[i].SetActive(false);
                liveChunks.RemoveAt(i);
                i--;
            }
        }
        liveChunks.Clear();
 
        for(int i = 0; i < chunksToMakeLive.Count; i++)
        {
            int chunkX = (int)chunksToMakeLive[i].x + chunkOffset ;
            int chunkY = (int)chunksToMakeLive[i].y + chunkOffset ;
            if (chunks[chunkX, chunkY] == null)
            {
                chunks[chunkX, chunkY] = new GameObject();
                chunks[chunkX, chunkY].name = "Chunk:" + (chunksToMakeLive[i].x ) + "," + (chunksToMakeLive[i].y );
                chunks[chunkX, chunkY].transform.parent = tileContainer.transform;
                CMChunk newChunk = chunks[chunkX, chunkY].AddComponent<CMChunk>();
                newChunk.x = (int)chunksToMakeLive[i].x ;
                newChunk.y = (int)chunksToMakeLive[i].y ;
                updatingTileList = true;
                CMChunkTiles newTiles = new CMChunkTiles();
                newTiles.chunk = newChunk;
                newTiles.nextTile = 0;
                tilesToGenerate.Add(newTiles);

                updatingTileList = false;
            }
            if (!chunks[chunkX, chunkY].activeSelf)
            {
                chunks[chunkX, chunkY].SetActive(true);
            }
            liveChunks.Add(chunks[chunkX, chunkY]);
        }
        
        //added cause it's faster to do this than debug. still getting WAY to many tiles to generate for some reason.
        needToCreateCount = tilesToGenerate.Count;

        if(!processingTiles && tilesToGenerate.Count > 0)
        {
            StartCoroutine("ProcessTiles");
        }
    }

    bool processingTiles = false;
    bool updatingTileList = false;

    System.Collections.IEnumerator ProcessTiles()
    {
        processingTiles = true;
        int updateCount = 100;
        float lastTime = Time.time;
        int startLength = tilesToGenerate.Count;
        int counter = 0;
        while (tilesToGenerate.Count > 1)
        {
            if(counter >= startLength)
            {
                break;
            }
            //if slower than 60fps, decrease the number of tiles that generate per frame.
            deltaTime = Time.deltaTime;
            if (deltaTime > .8f && !firstLoop)
            {
                if (updateCount > 10)
                {
                    updateCount -= 5;
                }
            }

            //if faster than 100fps, increase the number of tiles that generate per frame.
            if(deltaTime < .8f)
            {
                updateCount += 5;
            }
            if(updateCount > 300)
            {
                updateCount = 300;
            }
            for (int i = 0; i < updateCount; i++)
            {
                if (tilesToGenerate.Count > 0)
                {
                    CMChunk chunk = tilesToGenerate[0].chunk;
                    int tileIndex = tilesToGenerate[0].nextTile;
                    if (tileIndex == 0)
                    {
                        PrepareChunk(chunk);
                    }
                    GenerateTile(chunk, tileIndex);
                    tilesToGenerate[0].nextTile++;
                    if (tilesToGenerate[0].nextTile == chunkSize*chunkSize) // chunk finished
                    {
                        FinalizeChunk(chunk);
                        tilesToGenerate.RemoveAt(0);
                    }
                }
                
            }
            if (firstLoop)
            {
                yield return new WaitForEndOfFrame();
            }
        }
        firstLoop = true;
        processingTiles = false;
    }

    void PrepareChunk(CMChunk chunk)
    {
        chunk.tileBiomeID = new int[chunkSize, chunkSize];
        chunk.biomeIDCount = new int[biomesMerged.Count];
    }

    void GenerateTile(CMChunk chunk, int tileIndex)
    {
        int tileX = (tileIndex % chunkSize);
        int tileY = (tileIndex / chunkSize);
        int x = tileX + (chunk.x * chunkSize);
        int y = tileY + (chunk.y * chunkSize);
        
        //int biome = Mathf.FloorToInt(total / count);
        int biome = Mathf.FloorToInt(
                    Mathf.PerlinNoise(
                        ((((float)x+1000) * seed) / scale),
                        ((((float)y+1000) * seed) / scale)
                        )
                        * biomes.Count);
        //biome = Mathf.RoundToInt(((float)biome*mergeStrength) + (float)(biome2*(1-mergeStrength)));
        biome = Mathf.Clamp(biome, 0, biomes.Count - 1);
        try
        {
            int bID = biomesID[biome];
            chunk.tileBiomeID[tileX, tileY] = bID;
            chunk.biomeIDCount[bID]++;
            biomes[biome].GenerateTileObjects(seed,x, y, chunk);
        }
        catch(System.Exception ex)
        {
            Debug.Log("ERROR: " + biome + ", " + ex.Message);
        }
    }

    void FinalizeChunk(CMChunk chunk)
    {
        for (int bID = 0; bID < biomesMerged.Count; ++bID)
        {
            if (chunk.biomeIDCount[bID] > 0)
            {
                biomesMerged[bID].GenerateTiles(chunk.x * chunkSize, chunk.y * chunkSize, chunk,
                    chunkSize, bID, biomesMerged);
            }
        }
    }

    void UpdateBounds()
    {
        //possibly a pointless function at the moment.
        Vector3 bottomLeftPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.y));
        Vector3 topRightPosition = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, Camera.main.transform.position.y));

        screenBounds.left = Mathf.Floor(bottomLeftPosition.x);
        screenBounds.bottom = Mathf.Floor(bottomLeftPosition.z);
        screenBounds.right = Mathf.Floor(topRightPosition.x);
        screenBounds.top = Mathf.Floor(topRightPosition.z);
    }
}
public class CMBounds
{
    public float left;
    public float right;
    public float bottom;
    public float top;
}

public class CMChunkTiles
{
    public CMChunk chunk;
    public int nextTile;
}