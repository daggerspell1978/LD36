﻿using UnityEngine;
using System.Collections.Generic;


public class CMChunk : MonoBehaviour
{
    public int x = 0;
    public int y = 0;
    public int[,] tileBiomeID;
    public int[] biomeIDCount;
}
