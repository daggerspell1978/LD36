﻿using System;
using System.Collections;
using UnityEngine;

public class MusicController : MonoBehaviour {
    public AudioClip musicDaytime;
    public float musicDaytimeTargetVolume = .1f;
    public AudioClip musicNighttime;
    public float musicNighttimeTargetVolume = .1f;
    const float fadeSpeed = .002f;
    AudioSource audioSource;
    Coroutine routine;

    public void Start() {
        audioSource = GetComponent<AudioSource>();
        TimeManager.onDayTimeChanged += TimeManager_onDayTimeChanged;
        TimeManager_onDayTimeChanged(TimeManager.curTimeOfDay, TimeManager.curSeason);
    }

    private IEnumerator SwitchTracks(AudioClip nextClip, float targetVolume) {
        while(audioSource.volume > 0) {
            audioSource.volume = Math.Max(0, audioSource.volume - fadeSpeed);
            yield return 0;
        }

        audioSource.clip = nextClip;
        audioSource.Play();
        while(audioSource.volume < targetVolume) {
            audioSource.volume = Math.Max(0, audioSource.volume + fadeSpeed);
            yield return 0;
        }
    }

    void TimeManager_onDayTimeChanged(TimeOfDay _dayTime, Season _season) {
        AudioClip nextClip = null;
        float targetVolume = 1;
        switch(_dayTime) {
        case TimeOfDay.Morning:
            nextClip = musicDaytime;
            targetVolume = musicDaytimeTargetVolume;
            break;
        case TimeOfDay.Midday:
            break;
        case TimeOfDay.Evening:
            nextClip = musicNighttime;
            targetVolume = musicNighttimeTargetVolume;
            break;
        case TimeOfDay.Night:
            break;
        default:
            break;
        }

        if(nextClip != null) {
            if(routine != null) {
                StopCoroutine(routine);
            }
            routine = StartCoroutine(SwitchTracks(nextClip, targetVolume));
        }
    }
}