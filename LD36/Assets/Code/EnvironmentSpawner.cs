﻿using System.Collections;
using UnityEngine;

public class EnvironmentSpawner : MonoBehaviour {
    public GameObject beehive;
    public GameObject tree;

    GameObject SpawnBeehive(GameObject tree) {
        float x = Random.Range(0.75f, 1.1f);
        float y = 3.1f;
        float z = Random.Range(0.75f, 1.1f);
        Vector3 position = tree.transform.position + new Vector3(x, y, z);
        var newBeehive = (GameObject)Instantiate(beehive, position, Quaternion.identity);
        newBeehive.transform.SetParent(tree.transform);
        return newBeehive;
    }

    GameObject SpawnTree() {
        var x = Random.Range(-25, 25);
        var z = Random.Range(-25, 25);
        var newTree = (GameObject)Instantiate(tree, tree.transform.position + new Vector3(x, 0, z),
            Quaternion.identity);
        newTree.SetActive(true);
        newTree.transform.SetParent(transform);
        return newTree;
    }

    // Use this for initialization
    void Start() {
        for(int i = 0; i < 5; i++) {
            GameObject newTree = SpawnTree();
            if(Random.value > 0.4f)
                SpawnBeehive(newTree);
        }
    }

    // Update is called once per frame
    void Update() {
    }
}