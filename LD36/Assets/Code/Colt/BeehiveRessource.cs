﻿using UnityEngine;
using System.Collections;

public class BeehiveRessource : Ressource
{
    [Header("Honey")]
    public Fuel myHoney;

    public override void Awake()
    {
        base.Awake();

        if (myHoney != null)
            myHoney = Instantiate<Fuel>(myHoney);
        else
            myHoney = ScriptableObject.CreateInstance<Fuel>();
    }

    internal override void StartHarvestEffects()
    {
        base.StartHarvestEffects();
    }

    internal override void StopHarvestEffects()
    {
        base.StopHarvestEffects();
    }

    protected override void HarvestActionComplete(BasicWorker worker)
    {
        Debug.Assert(worker != null);
        Debug.Assert(worker.inventory != null);
        base.HarvestActionComplete(worker);
        worker.inventory.AddItem(myHoney);
        print("harvested honey");
        HarvestCompleteNoLoot();
    }
}
