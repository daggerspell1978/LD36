﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This is not being used anymore
/// </summary>
public class EnvironmentController : EntityController {

    public override void Awake() {
        base.Awake();
        MouseController.environment.Add(this);
    }

    public void OnDisable() {
        MouseController.environment.Remove(this);
    }
}