﻿using System.Collections;
using UnityEngine;

public class BeeSwarm : MonoBehaviour {
    public bool dead = false;
    public float fadeTime = 2.5f;
    NavMeshAgent agent;
    private float chaseTime = 20f;
    private float currparticleFade = 0.001f;
    private float particleFade = 0.15f;
    ParticleSystem particles;
    float scareDistance = 3f;
    BasicWorker target;
    public bool IsMoving {
        get {
            return agent.velocity.magnitude > 0;
        }
    }

    public Vector3 ScarePosition() {
        Vector3 direction = (target.transform.position - transform.position).normalized;
        direction.y = 0;
        return target.transform.position + (direction * 7);
    }

    public void SetTarget(BasicWorker _target) {
        target = _target;
    }

    void Kill() {
        Destroy(gameObject);
    }

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        particles = GetComponent<ParticleSystem>();
    }

    void Update() {
        if(chaseTime > 0) {
            float distance = (transform.position - target.transform.position).magnitude;
            if(distance < scareDistance) {
                target.myMovement.MoveTo(ScarePosition());
                target.curRessource = null;
            }

            if(distance <= Random.Range(0, 2f))
                agent.Stop();

            if(!IsMoving) {
                agent.SetDestination(target.transform.position);
                agent.Resume();
            }

            chaseTime -= Time.deltaTime;
        } else {
            if(currparticleFade <= 0) {
                particles.maxParticles -= 1;
                currparticleFade = particleFade;
            }

            if(particles.maxParticles <= 0)
                Kill();

            currparticleFade -= Time.deltaTime;
        }
    }
}