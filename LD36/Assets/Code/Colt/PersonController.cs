﻿using System;
using System.Collections;
using UnityEngine;

public class PersonController : EntityController {
    public NavMeshAgent agent;
    Coroutine routine;
    float speed = .1f;

    float scared = 0;


    public override void Awake() {
        base.Awake();
        agent = GetComponent<NavMeshAgent>();
        MouseController.people.Add(this);
    }

    public void OnDisable() {
        MouseController.people.Remove(this);
    }

    void Update()
    {
        if (scared > 0)
            scared -= Time.deltaTime;
        else if (scared < 0) scared = 0;

        Collider[] colliders = Physics.OverlapSphere(transform.position, 2);
        for (int i = 0; i < colliders.Length; ++i)
        {
            Triggered(colliders[i]);
        }
    }

    public void Triggered(Collider other)
    {
        if (Frighten(other))
            return;

        var tree = other.GetComponent<Tree>();
        if (tree != null)
        {
            tree.Chop();
        }
    }

    bool Frighten(Collider other)
    {
        if (scared != 0)
            return true;

        var ghost = other.GetComponentInChildren<Ghost>();
        
        if (ghost != null)
        {
            ghost.Boo(transform.position);
            Debug.Log("Scared!");
            scared = 5;
            NavMeshHit newDestination;
            NavMesh.SamplePosition(transform.position + (new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * 7), out newDestination, 10, 1);
            agent.SetDestination(newDestination.position);
            return true;
        }

        return false;
    }

    internal void InteractWith(EnvironmentController selectedEnvironment, int buttonId) {
        if(routine != null) {
            StopCoroutine(routine);
        }
        //routine = StartCoroutine(GoTo(selectedEnvironment));
        agent.SetDestination(selectedEnvironment.transform.position);
    }

    //private IEnumerator GoTo(EnvironmentController selectedEnvironment) {
    //    var deltaPosition = transform.position - selectedEnvironment.transform.position;
    //    while(deltaPosition.magnitude > 3f) {
    //        transform.position = Vector3.MoveTowards(transform.position, selectedEnvironment.transform.position, speed);
    //        yield return 0;
    //        deltaPosition = transform.position - selectedEnvironment.transform.position;
    //    }
    //}
}