﻿using System.Collections;
using UnityEngine;

public class Beehive : MonoBehaviour
{
    public GameObject BeeSwarm;

    [Header("Honey")]
    public Fuel myHoney;

    public AudioClip harvestSound;
    public float harvestSoundVolume;

    public void Fall(BasicWorker worker)
    {
        transform.SetParent(GetEnvironmentInteractableBase(transform.parent));
        Rigidbody rb = GetComponent<Rigidbody>();
        if (!rb.useGravity)
        {
            rb.useGravity = true;
            rb.isKinematic = false;
        }

        Destroy(GetComponent<ParticleSystem>());
        Destroy(this);
        BeehiveRessource res = gameObject.AddComponent<BeehiveRessource>();
        res.myHoney = myHoney;
        res.harvestSound = harvestSound;
        res.harvestSoundVolume = harvestSoundVolume;

        GameObject obj = (GameObject) Instantiate(BeeSwarm, transform.position, Quaternion.identity);
        BeeSwarm swarm = obj.GetComponent<BeeSwarm>();
        swarm.SetTarget(worker);
    }

    Transform GetEnvironmentInteractableBase(Transform trans)
    {
        if(trans == null || trans.name == "EnvironmentInteractables")
            return trans;
        return GetEnvironmentInteractableBase(trans.parent);
    }
}