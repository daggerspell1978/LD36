﻿using System.Collections;
using UnityEngine;

public class TorchLit : MonoBehaviour {
    /*
    Torch must have a light component
    */

    public float timeToStayLit;
    Light myLight;

    //relight the torch
    public void Relight() {
        timeToStayLit = Random.Range(10f, 50f);
        myLight.intensity = timeToStayLit / 100;
    }

    void DecreaceLighting() {
        myLight.intensity -= (timeToStayLit / 100) * Time.deltaTime;
    }

    void Start() {
        myLight = transform.FindChild("FireAndLight").GetComponentInChildren<Light>();
        Relight();
    }

    void Update() {
        DecreaceLighting();
    }
}