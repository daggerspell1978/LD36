﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VideoTest : MonoBehaviour {
    public GameObject cube;
    public RawImage pcMovie;
#if UNITY_STANDALONE || UNITY_EDITOR
    public MovieTexture movie;
#endif
    WebGLMovieTexture tex;

    IEnumerator ChangeSceneAfterMovie() {
#if UNITY_STANDALONE
        if(movie != null)
        {
            pcMovie.texture = movie;
            movie.Play();
            while (movie.isPlaying) yield return new WaitForEndOfFrame();
        }
#else
        yield return new WaitForSeconds(80);
#endif

        SceneManager.LoadScene(1);
    }

    void Start() {
#if UNITY_STANDALONE

        pcVersion.gameObject.SetActive(false);
#else
        tex = new WebGLMovieTexture("StreamingAssets/FinishedVideo.mp4");
        cube.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
        cube.GetComponent<MeshRenderer>().material.mainTexture = tex;
        tex.Play();
#endif
        StartCoroutine(ChangeSceneAfterMovie());
    }

    void Update() {
#if UNITY_STANDALONE

#else
        tex.Update();
#endif
    }
}