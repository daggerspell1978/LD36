﻿using UnityEngine;
using System.Collections;

public class cameraController : MonoBehaviour {
    //Author InternetsLIVE
    //Basic camera script / rotate
    private float mainSpeed = 15.0f;
    void Update () {
        Vector3 keyVelocity = GetInput();
        Vector3 keyRotate = GetRotate();
        keyVelocity = keyVelocity * mainSpeed;
        keyVelocity = keyVelocity * Time.deltaTime;
        Vector3 newPosition = gameObject.transform.position;
        gameObject.transform.Translate(keyVelocity);
        gameObject.transform.Rotate(keyRotate);
    }

    private Vector3 GetInput()
    { 
        Vector3 velocity = new Vector3();
        
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            velocity += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            velocity += new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            velocity += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            velocity += new Vector3(1, 0, 0);
        }
        return velocity;
    }
    private Vector3 GetRotate()
    {
        Vector3 rotate = new Vector3();
        if ((Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift)) && Input.GetKey(KeyCode.Mouse2)){
            rotate += new Vector3(0, 1, 0);
        }
        else if (Input.GetKey(KeyCode.Mouse2)){ 
            rotate -= new Vector3(0, 1, 0);
        }
       
        return rotate;
    }
}
