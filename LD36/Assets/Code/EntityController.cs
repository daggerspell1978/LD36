﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class EntityController : MonoBehaviour {
    [NonSerialized]
    public GameObject selectedUI;

    public virtual void Awake() {
        selectedUI = transform.FindChild("Selected").gameObject;
        selectedUI.SetActive(false);
    }
}