﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class CampFireFlickerLight : MonoBehaviour {
    public Color colorA;
    public Color colorB;
    public float colorChangeSpeed = 0.5f;
    public float flickerSpeed = 2;
    public float maxFlickerIntensity = 2f;
    public float minFlickerIntensity = 0.25f;
    public float rndSeed = 420;

    public Light thisLight;

    void Awake() {
        colorA = hexToColor("FFD70000");
        colorB = hexToColor("FFAE0000");
        rndSeed = Random.Range(0.0f, 65535.0f);
    }

    private string colorToHex(Color32 color) {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    private Color hexToColor(string hex) {
        hex = hex.Replace("0x", "");
        hex = hex.Replace("#", "");
        byte a = 255;
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        if(hex.Length == 8) {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }

    void Update() {
        float noise = Mathf.PerlinNoise(rndSeed, Time.time * flickerSpeed);
        thisLight.intensity = Mathf.Lerp(minFlickerIntensity, maxFlickerIntensity, noise);

        float t = Mathf.PingPong(Time.time, colorChangeSpeed) / colorChangeSpeed;
        thisLight.color = Color.Lerp(colorA, colorB, t);
    }
}