﻿/* 
 *    @Scryptonite
 * 
 * Just attach this component to any GameObject with a Collider.
 *
 * If you end up using my cursor set, the hotspot is pretty good at <8, 7>.
 * 
 * Texture Configuration Settings:
 *    Texture Type: Cursor
 *    Wrap Mode: Clamp
 *    Filter Mode: Trilinear
 *    Default: 
 *      Max Size: 32
 *      Format: Truecolor
 *
 * Don't forget to update the project's default cursor and hotspot under :
 *   Edit > Project Settings > Player
 *
*/

using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class ChangeCursor : MonoBehaviour {
    public Texture2D mouseOverCursor;
    public Vector2 hotspot = new Vector2(8, 7);
    
    void OnMouseEnter() {
        Cursor.SetCursor(mouseOverCursor, hotspot, CursorMode.Auto);
    }

	void OnMouseExit() {
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
	}
}