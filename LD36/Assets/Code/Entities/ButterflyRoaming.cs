﻿using System.Collections;
using UnityEngine;

public class ButterflyRoaming : MonoBehaviour {
    [Header("FlySpeed")]
    public float flySpeed = 1f;

    public float maxX = 10f;
    public float maxY = 10f;
    public float maxZ = 10f;
    [Header("Borders")]
    public float minX = -10f;
    public float minY = 0.5f;
    public float minZ = -10f;
    private Vector3 newPos;
    Renderer[] renderers;

    void GetNewPos() {
        newPos = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ));
    }

    void Start() {
        GetNewPos();
        renderers = GetComponentsInChildren<Renderer>();
        TimeManager.onSeasonChanged += TimeManager_onSeasonChanged;
    }

    private void TimeManager_onSeasonChanged(TimeOfDay _dayTime, Season _season) {
        if(_season == Season.Winter) {
            for(int i = 0; i < renderers.Length; i++) {
                renderers[i].enabled = false;
            }
        } else if(_season == Season.Spring) {
            for(int i = 0; i < renderers.Length; i++) {
                renderers[i].enabled = true;
            }
        }
    }

    void Update() {
        transform.LookAt(newPos);
        transform.Translate(Vector3.forward * (flySpeed * Time.deltaTime));

        if(Vector3.Distance(this.transform.position, newPos) <= 1f)
            GetNewPos();
    }
}