﻿using System.Collections;
using UnityEngine;

//by mcrannch
//simple script for moving the bunny
public class BunnyMovment : MonoBehaviour {
    public float hopHeight = .25f;
    private float currentCoolDown = 0.25f;
    private float hopCoolDown = .25f;
    private float speed = 2.0f;
    private Vector3 target;

    //sets a new target for the bunny to head to
    private void setTarget() {
        target = new Vector3(Random.Range(2.0f, 30.0f), 0.0f, Random.Range(2.0f, 30.0f));
    }

    void Start() {
        setTarget();
    }

    void Update() {
        currentCoolDown -= Time.deltaTime;
        this.gameObject.transform.LookAt(target);
        this.gameObject.transform.Translate(Vector3.forward * (speed * Time.deltaTime));

        //sometimes we need to reset the y to keep the bunny from jumping into the ground

        if(currentCoolDown <= 0) {
            if(this.gameObject.transform.position.y > hopHeight) {
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, transform.position.y - hopHeight, this.gameObject.transform.position.z);
            } else {
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, transform.position.y + hopHeight, this.gameObject.transform.position.z);
            }
            currentCoolDown = hopCoolDown;
        }

        if(Vector3.Distance(this.gameObject.transform.position, target) <= 2f) {
            setTarget();
        }
    }
}