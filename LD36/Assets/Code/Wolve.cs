﻿using System.Collections;
using UnityEngine;

public class Wolve : MonoBehaviour {
    public bool hasTarget = false;
    public float speed = 5;
    public Transform target = null;
    Vector3 midpoint = new Vector3(0, 1000, 0);
    Vector3 roamPos = new Vector3(0, 1000, 0);

    void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "worker" && target == null) {
            target = collision.transform;
            hasTarget = true;
            roamPos = new Vector3(0, 1000, 0);
        } else if(collision.gameObject.tag == "fire" || collision.gameObject.tag == "Campfire" || collision.gameObject.tag == "torch") {
            if(hasTarget && target != null) {
                print("new midpoind");
                midpoint = transform.position + new Vector3(Random.Range(-10, 10), transform.position.y, Random.Range(-10, 10));
            } else {
                hasTarget = false;
                roamPos = new Vector3(transform.position.x + Random.Range(-10, 10), transform.position.y, transform.position.z + Random.Range(-10, 10));
            }
        }
    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        float step = speed * Time.deltaTime;

        if(Vector3.Distance(transform.position, midpoint) < 0.5f)
            midpoint = new Vector3(0, 1000, 0);

        if(hasTarget && target != null && Vector3.Distance(target.position, transform.position) <= 1) {
            // eat the worker
            Debug.Log("I'm gonna eat you!!!!");
        }

        //have target but going to midpoint
        if(hasTarget && midpoint.y != 1000) {
            Debug.DrawLine(transform.position, midpoint, Color.red);
            transform.LookAt(midpoint);
            transform.position = Vector3.MoveTowards(transform.position, midpoint, step);
            return;
        }

        // have target
        if(hasTarget && midpoint.y == 1000) {
            Debug.DrawLine(transform.position, target.position, Color.yellow);
            transform.LookAt(target.position);
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
            return;
        }

        if(!hasTarget) {
            if(roamPos.y == 1000 || transform.position == roamPos)
                roamPos = new Vector3(transform.position.x + Random.Range(-10, 10), transform.position.y, transform.position.z + Random.Range(-10, 10));
            transform.LookAt(roamPos);
            Debug.DrawLine(transform.position, roamPos, Color.green);
            transform.position = Vector3.MoveTowards(transform.position, roamPos, step * 0.5f);
            return;
        }
    }
}